//
//  UserService.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

import Foundation

class UserService: UserServicing {
    
    // MARK: - Private mocked data
    fileprivate let firstNames = ["Burley", "Providenci", "Alan", "Jazmyn", "Caterina", "Garfield", "Annabell", "Aimee", "Rosina", "Delfina", "Rylan", "Karolann", "Kiana", "Juliana", "Elissa", "Collin", "Raven", "Ruby", "Hallie", "Sierra"]
    
    fileprivate let lastNames = ["Funk", "Cartwright", "Schulist", "Dietrich", "Klocko", "Beier", "Legros", "Borer", "Bartell", "Johns", "Lehner", "Dare", "Shields", "Buckridge", "Homenick", "Schmidt", "Mann", "Haag", "Ernser", "Mertz"]
    
    fileprivate let descriptions = ["Mollitia fugiat amet ipsam aut dolor quas qui doloribus architecto.", "Possimus in nobis quis quod laudantium et.", "Ex maxime minima et laboriosam aut dolor.", "Qui autem consequuntur corrupti delectus qui nihil voluptas perferendis in.", "Adipisci temporibus et voluptatem reprehenderit unde.", "Rerum hic animi consequatur tenetur suscipit itaque ut.", "Corporis fugiat voluptas quia ducimus et.", "Esse molestiae id.", "Maxime voluptas dolores.", "Perferendis est porro aspernatur recusandae quasi eum.", "Eius incidunt et harum qui eveniet veritatis maiores rem placeat.", "Fugit quibusdam voluptate itaque delectus.", "Delectus illo ratione architecto reprehenderit et.", "Culpa labore ut consequatur.", "Omnis aut vitae est.", "Voluptas pariatur ipsam voluptatem illo numquam rem enim atque."]
    
    // MARK: - Deinitializator
    deinit {
        print("\(String(describing: UserService.self)) deinit successfully")
    }
    
    // MARK: - Public methods
    func fetch(completion: @escaping (Result<[User], Error>) -> Void) {
        DispatchQueue.global().async {
            completion(.success(self.mockedUsers))
        }
    }
    
    func getDetails(_ to: User, completion: @escaping (Result<User, Error>) -> Void) {
        DispatchQueue.global().async {
            var to = to
            to.about = self.descriptions.randomElement()
            completion(.success(to))
        }
    }
    
    // MARK: - Private properties
    fileprivate var mockedCounters = 0
    
    // MARK: - Private methods
    fileprivate var mockedUsers: [User] {
        mockedCounters += 1
        if mockedCounters.isMultiple(of: 3) {
            return []
        }
        
        return (0..<15).map { _ in
            User(firstName: firstNames.randomElement(), lastName: lastNames.randomElement())
        }
    }
}
