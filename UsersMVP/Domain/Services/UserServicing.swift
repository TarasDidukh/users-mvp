//
//  UserServicing.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

protocol UserServicing {
    func fetch(completion: @escaping (Result<[User], Error>) -> Void)
    func getDetails(_ to: User, completion: @escaping (Result<User, Error>) -> Void)
}
