//
//  AppCoordinator.swift
//  UsersMVP
//
//  Created by Taras Didukh on 02.10.2020.
//

import UIKit

struct AppCoordinator: Coordinator {
    // MARK: - Properties
    let window: UIWindow
    let router: UINavigationController
    let usersCoordinator: Coordinator

    // MARK: - Initializer
    init(window: UIWindow) {
        self.window = window
        router = UINavigationController()
        usersCoordinator = UsersCoordinator(router: router)
    }
    
    // MARK: - Navigation
    func start() {
        window.rootViewController = router
        usersCoordinator.start()
        window.makeKeyAndVisible()
    }
}
