//
//  UserDetailsCoordinator.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

import UIKit

protocol UserDetailsCoordinating {
    func dismiss()
}

class UserDetailsCoordinator: UserDetailsCoordinating {
    // MARK: - Properties
    private let router: UINavigationController
    
    // MARK: - Initializer
    init(router: UINavigationController) {
        self.router = router
    }
    
    // MARK: - Navigation
    func dismiss() {
        router.dismiss(animated: true, completion: nil)
        //router.popViewController(animated: true)
    }
}
