//
//  UsersCoordinator.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

import UIKit

protocol UsersCoordinatoring {
    func showUserDetails(user: User, delegate: UserDetailsPresenterDelegate)
}

struct UsersCoordinator: UsersCoordinatoring, Coordinator {
    // MARK: - Properties
    private let router: UINavigationController
    
    // MARK: - Initializer
    init(router: UINavigationController) {
        self.router = router
    }
    
    // MARK: - Navigation
    func start() {
        let usersController = UsersController(nibName: "UsersController", bundle: nil)
        usersController.presenter = UsersPresenter(view: usersController, coordinator: self, userService: UserService())
        router.pushViewController(usersController, animated: true)
    }
    
    func showUserDetails(user: User, delegate: UserDetailsPresenterDelegate) {
        let userDetailsController = UserDetailsController()
        userDetailsController.modalPresentationStyle = .formSheet
        userDetailsController.presenter = UserDetailsPresenter(user: user, view: userDetailsController, userService: UserService(), coordinator: UserDetailsCoordinator(router: router), delegate: delegate)
       
        router.present(userDetailsController, animated: true, completion: nil)
        
        //TODO: deinit testing purpose
        //router.setViewControllers([userDetailsController], animated: true)
    }
}
