//
//  Coordinator.swift
//  UsersMVP
//
//  Created by Taras Didukh on 02.10.2020.
//
import UIKit

protocol Coordinator {
    func start()
}
