//
//  UsersPresenter.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

import Foundation
class UsersPresenter: UsersPresenting {
    // MARK: - Properties
    fileprivate weak var view: UsersView?
    fileprivate let coordinator: UsersCoordinatoring
    fileprivate let userService: UserServicing
    
    // TODO: Better to implement CellView, left for simplicity
    fileprivate var users: [User] = []
    
    // MARK: - Initializer
    init(view: UsersView,
         coordinator: UsersCoordinatoring,
         userService: UserServicing) {
        self.view = view
        self.coordinator = coordinator
        self.userService = userService
    }
    
    // MARK: - Initializer
    deinit {
        print("\(String(describing: UserDetailsPresenter.self)) deinit successfully")
    }
    
    // MARK: - Business logic
    var usersCount: Int {
        users.count
    }
    
    func userAt(_ index: Int) -> User? {
        if index >= 0 && index < users.count {
            return users[Int(index)]
        }
        return nil
    }
    
    func refresh() {
        view?.beginBusy()
        userService.fetch { result in
            DispatchQueue.main.async {
                self.view?.finishBusy()
                if case .success(let users) = result {
                    self.users = users
                    users.isEmpty ? self.view?.setEmpty() : self.view?.update()
                }
                
                // TODO: implement error handler, skipped for now
            }
        }
    }
    
    func didSelect(_ at: Int) {
        guard let user = userAt(at) else { return }
        coordinator.showUserDetails(user: user, delegate: self)
    }
    
}

extension UsersPresenter: UserDetailsPresenterDelegate {
    func save(didSave user: User) {
        if let idx = users.firstIndex(where: { $0 == user }) {
            users[idx] = user
            self.view?.update(idx)
        }
    }
}
