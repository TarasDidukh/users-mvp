//
//  UserPresenting.swift
//  UsersMVP
//
//  Created by Taras Didukh on 02.10.2020.
//

protocol UsersPresenting {
    var usersCount: Int { get }
    
    func userAt(_ index: Int) -> User?
    func refresh()
    func didSelect(_ at: Int)
}
