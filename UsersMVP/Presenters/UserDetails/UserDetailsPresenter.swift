//
//  UserDetailsPresenter.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

import Foundation


protocol UserDetailsPresenterDelegate: class {
    func save(didSave user: User)
}

class UserDetailsPresenter: UserDetailsPresenting {
    // MARK: - Properties
    fileprivate weak var view: UserDetailsView?
    fileprivate weak var delegate: UserDetailsPresenterDelegate?
    fileprivate let userService: UserServicing
    fileprivate let coordinator: UserDetailsCoordinating
    fileprivate var user: User
    
    // MARK: - Initializer
    init(user: User,
         view: UserDetailsView,
         userService: UserServicing,
         coordinator: UserDetailsCoordinating,
         delegate: UserDetailsPresenterDelegate) {
        self.view = view
        self.delegate = delegate
        self.userService = userService
        self.coordinator = coordinator
        self.user = user
    }
    
    // MARK: - Deinitializator
    deinit {
        print("\(String(describing: UserDetailsPresenter.self)) deinit successfully")
    }
    
    // MARK: - Business logic
    func fetchDetails() {
        userService.getDetails(user) { result in
            DispatchQueue.main.async {
                if case .success(let user) = result {
                    self.user = user
                    self.view?.display(user.firstName, user.lastName)
                }
                // TODO: implement error handler, skipped for now
            }
        }
    }
    
    func save(_ firstName: String?, _ lastName: String?) {
        // TODO: call external storage, skip for now
        
        user.firstName = firstName
        user.lastName = lastName
        coordinator.dismiss()
        delegate?.save(didSave: user)
    }
    
    func cancel() {
        coordinator.dismiss()
    }
}
