//
//  UserDetailsPresenting.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

protocol UserDetailsPresenting: class {
    func fetchDetails()
    func save(_ firstName: String?, _ lastName: String?)
    func cancel()
}
