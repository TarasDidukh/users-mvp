//
//  User.swift
//  UsersMVP
//
//  Created by Taras Didukh on 02.10.2020.
//
import Foundation

struct User {
    let id: String = UUID().uuidString
    var firstName: String?
    var lastName: String?
    var about: String?
    
    var info: String {
        firstName?.isEmpty != false ? "Anonymous" : firstName!
    }
}

extension User: Equatable {
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.id == rhs.id
    }
}


