//
//  AppDelegate.swift
//  UsersMVP
//
//  Created by Taras Didukh on 02.10.2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    private var appCoordinator: AppCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        
        self.window = window
        appCoordinator = AppCoordinator(window: window)
        
        appCoordinator?.start() 
        return true
    }

}

