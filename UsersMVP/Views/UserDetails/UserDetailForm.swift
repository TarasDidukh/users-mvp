//
//  UserDetailForm.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

import UIKit

class UserDetailForm: UIView {
    // MARK: - Dimensions
    let sideMargin: CGFloat = 16
    let topMargin: CGFloat = 20
    let verticalSpacing: CGFloat = 8
    let horizontalSpacing: CGFloat = 8
    
    let btnVerticalInset: CGFloat = 8
    let btnHorizontalInset: CGFloat = 16
    
    // MARK: - UI elements
    
    var tfFirstName: UITextField!
    var tfLastName: UITextField!
    var btnSave: UIButton!
    var btnCancel: UIButton!
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    // MARK: - UI builders
    
    private func setupView() {
        setupFirstName()
        setupLastName()
        setupSaveButton()
        setupCancelButton()
    }
    
    fileprivate func setupFirstName() {
        tfFirstName = UITextField()
        tfFirstName.placeholder = "First Name"
        tfFirstName.borderStyle = .bezel
        addSubview(tfFirstName)
        tfFirstName.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            tfFirstName.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: sideMargin),
            tfFirstName.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -sideMargin),
            tfFirstName.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: topMargin)
        ])
    }
    
    fileprivate func setupLastName() {
        tfLastName = UITextField()
        tfLastName.placeholder = "Last Name"
        tfLastName.borderStyle = .bezel
        addSubview(tfLastName)
        tfLastName.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            tfLastName.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: sideMargin),
            tfLastName.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -sideMargin),
            tfLastName.topAnchor.constraint(equalTo: tfFirstName.bottomAnchor, constant: verticalSpacing)
        ])
    }
    
    fileprivate func setupSaveButton() {
        btnSave = UIButton(type: .system)
        btnSave.setTitle("Save", for: .normal)
    
        btnSave.contentEdgeInsets = UIEdgeInsets(top: btnVerticalInset, left: btnHorizontalInset, bottom: btnVerticalInset, right: btnHorizontalInset)
        addSubview(btnSave)
        btnSave.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            btnSave.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -sideMargin),
            btnSave.topAnchor.constraint(equalTo: tfLastName.bottomAnchor, constant: verticalSpacing)
        ])
    }
    
    fileprivate func setupCancelButton() {
        btnCancel = UIButton(type: .system)
        btnCancel.setTitle("Cancel", for: .normal)
    
        btnCancel.contentEdgeInsets = UIEdgeInsets(top: btnVerticalInset, left: btnHorizontalInset, bottom: btnVerticalInset, right: btnHorizontalInset)
        addSubview(btnCancel)
        btnCancel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            btnCancel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: sideMargin),
            btnCancel.trailingAnchor.constraint(equalTo: btnSave.leadingAnchor, constant: horizontalSpacing),
            btnCancel.topAnchor.constraint(equalTo: tfLastName.bottomAnchor, constant: verticalSpacing),
            btnCancel.widthAnchor.constraint(equalTo: btnSave.widthAnchor)
        ])
    }
    
    func fill(_ firstName: String?, _ lastName: String?) {
        tfFirstName.text = firstName
        tfLastName.text = lastName
    }
}

