//
//  UserDetailView.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

protocol UserDetailsView: class {
    func display(_ firstName: String?, _ lastName: String?)
}
