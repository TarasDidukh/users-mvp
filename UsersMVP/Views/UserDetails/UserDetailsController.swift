//
//  UserDetailController.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

import UIKit

class UserDetailsController: UIViewController {
    // MARK: - Properties
    var presenter: UserDetailsPresenting! {
        didSet {
            presenter.fetchDetails()
        }
    }
    
    var customView: UserDetailForm {
        view as! UserDetailForm
    }
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "User Details"
        
        customView.btnSave.addTarget(self, action: #selector(onSave), for: .touchUpInside)
        customView.btnCancel.addTarget(self, action: #selector(onCancel), for: .touchUpInside)
    }
    
    override func loadView() {
        let view = UserDetailForm()
        view.backgroundColor = .white
        self.view = view
    }
    
    deinit {
        print("\(String(describing: UserDetailsController.self)) deinit successfully")
    }
    
    // MARK: - Event handlers
    
    @objc func onSave() {
        let view = customView
        presenter.save(view.tfFirstName.text, view.tfLastName.text)
    }
    
    @objc func onCancel() {
        presenter.cancel()
    }
}

extension UserDetailsController: UserDetailsView {
    func display(_ firstName: String?, _ lastName: String?) {
        customView.fill(firstName, lastName)
    }
}


