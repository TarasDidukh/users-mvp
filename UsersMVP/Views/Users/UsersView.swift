//
//  UsersView.swift
//  UsersMVP
//
//  Created by Taras Didukh on 02.10.2020.
//

import Foundation
protocol UsersView: class {
    func beginBusy()
    func finishBusy()
    func setEmpty()
    func update()
    func update(_ at: Int)
}
