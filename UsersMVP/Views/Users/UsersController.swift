//
//  UsersController.swift
//  UsersMVP
//
//  Created by Taras Didukh on 03.10.2020.
//

import UIKit

class UsersController: UITableViewController {
    // MARK: - Properties
    let cellId = "userCell"
    
    var presenter: UsersPresenting!
    
    var lbEmpty: UILabel!
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Users"

        setupTable()
        setupEmptyLabel()
        onRefresh()
    }
    
    deinit {
        print("\(String(describing: UsersController.self)) deinit successfully")
    }
    
    // MARK: - UI initializators
    
    fileprivate func setupTable() {
        tableView.tableFooterView = UIView()
        refreshControl = UIRefreshControl()
        
        refreshControl?.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
    }
    
    fileprivate func  setupEmptyLabel() {
        lbEmpty = UILabel()
        lbEmpty.text = "Users not exist"
        lbEmpty.sizeToFit()
    }
    
    // MARK: - Event handlers
    
    @objc func onRefresh() {
        presenter.refresh()
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.usersCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellId)
        }
        
        cell.textLabel?.text = presenter.userAt(indexPath.row)?.info
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.didSelect(indexPath.row)
    }
}

extension UsersController: UsersView {
    // MARK: - UsersView
    func beginBusy() {
        let refresher = refreshControl!
        if !refresher.isRefreshing {
            tableView.setContentOffset(CGPoint(x: 0, y: -refresher.frame.size.height), animated: true)
            refresher.beginRefreshing()
        }
    }
    
    func finishBusy() {
        refreshControl?.endRefreshing()
    }
    
    func setEmpty() {
        tableView.tableHeaderView = lbEmpty
        tableView.reloadData()
    }
    
    func update() {
        tableView.tableHeaderView = nil
        tableView.reloadData()
    }
    
    func update(_ at: Int) {
        tableView.reloadRows(at: [IndexPath(row: at, section: 0)], with: .automatic)
    }
}
